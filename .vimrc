"général setup
set nocompatible
filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'flazz/vim-colorschemes'
Plugin 'scrooloose/nerdtree'
Plugin 'AndrewRadev/switch.vim'
Plugin 'tpope/vim-rails.git'
Plugin 'L9'
Plugin 'bling/vim-airline'
Plugin 'gcmt/taboo.vim'
Plugin 'nanotech/jellybeans.vim'
Plugin 'tpope/vim-vividchalk'
Plugin 'endel/vim-github-colorscheme'
Plugin 'goatslacker/mango.vim'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'kien/ctrlp.vim'
Plugin 'godlygeek/tabular'
Plugin 'tpope/vim-surround'
Plugin 'slim-template/vim-slim'
Plugin 'kchmck/vim-coffee-script'
Plugin 'tpope/vim-cucumber'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'tpope/vim-abolish'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'
Plugin 'honza/vim-snippets'
Plugin 'tpope/vim-fugitive'
Plugin 'w0ng/vim-hybrid'
Plugin 'gkz/vim-ls'
Plugin 'pbrisbin/vim-mkdir'
Plugin 'danro/rename.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'scrooloose/syntastic'
Plugin 'ecomba/vim-ruby-refactoring'
Plugin 'vim-scripts/matchit.zip'
Plugin 'plasticboy/vim-markdown'
Plugin 'kana/vim-textobj-user'
Plugin 'glts/vim-textobj-indblock'
Plugin 'nelstrom/vim-textobj-rubyblock'
Plugin 'dkprice/vim-easygrep'
Plugin 'xolox/vim-misc'
Plugin 'xolox/vim-reload'
Plugin 'mileszs/ack.vim'
Plugin 'AndrewRadev/coffee_tools.vim'
Plugin 'ranska/vim-coffeescript-refactoring'
Plugin 'jiangmiao/auto-pairs'
Plugin 'christoomey/vim-tmux-runner'
Plugin 'vim-scripts/LanguageTool'
Plugin 'tomtom/tinykeymap_vim'
Plugin 'jgdavey/vim-blockle'
Plugin 'AndrewRadev/splitjoin.vim'
Plugin 'rainerborene/vim-reek'
Plugin 'ngmy/vim-rubocop'
Plugin 'ingo-library'
Plugin 'clone'
Plugin 't9md/vim-choosewin'
Plugin 'ap/vim-css-color'
Plugin 'josuecau/ctrlsf.vim'
Plugin 'tpope/vim-projectionist'
Plugin 'unegunn/vim-emoji'
Plugin 'itchyny/calendar.vim'
Plugin 'szw/vim-tags'
Plugin 'shaoqiu/ctrlp-tags.git'
Plugin 'shaoqiu/vim-tools.git'
Plugin 'michaeljsmith/vim-indent-object'
Plugin 'jneen/ragel.vim'

call vundle#end()

" Vim setup
filetype plugin indent on
set number
set history=50
set ruler
set showcmd
set incsearch
set tabstop=2
set shiftwidth=2
set laststatus=2
set expandtab
set nolist listchars=tab:»·,trail:·
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/

" Colorshemes setup
colorscheme molokai

" NerdTree setup
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeWinPos = "right"
try
autocmd vimenter * NERDTree
catch
endtry

" Switch setup
let g:switch_mapping = "-"
