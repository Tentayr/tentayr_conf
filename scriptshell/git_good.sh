#!/bin/bash

echo "Let's Go !"

check_branch_exist () {
  if [ $? -eq 0 ];
    then
      echo succes
    else
      nein_nein_nein
  fi
}

nein_nein_nein () {
echo
echo "///////       ///   ////////////   ///   ///////       ///   ///"
echo "/// ////      ///   ////////////   ///   /// ////      ///   ///"
echo "///  ////     ///   ///            ///   ///  ////     ///   ///"
echo "///   ////    ///   ////////////   ///   ///   ////    ///   ///"
echo "///    ////   ///   ////////////   ///   ///    ////   ///   ///"
echo "///     ////  ///   ///            ///   ///     ////  ///   ///"
echo "///      //// ///   ////////////   ///   ///      //// ///      "
echo "///       ///////   ////////////   ///   ///       ///////   ///"
}

current_branch=$(git symbolic-ref HEAD --short)
check_branch_exist
