#!/bin/bash
echo "Choississez le Mappage du Clavier [vim/norm] : "
read -r value
if [ "$value" = "vim" ]; then
  xmodmap -e "keycode 9 = Caps_Lock Escape Escape Escape"
  xmodmap -e "keycode 66 = Escape Caps_Lock Caps_Lock Caps_Lock"
  xmodmap -e "keycode 48 = acute percent ugrave ugrave"
  echo "Clavier mapper en mode vim !"
  exit 0
elif [ "$value" = "norm" ]; then
  xmodmap -e "keycode 9 = Escape Escape Escape Escape"
  xmodmap -e "keycode 66 = Caps_Lock Caps_Lock Caps_Lock Caps_Lock"
  xmodmap -e "keycode 48 = ugrave percent ugrave dead_acute"
  echo "Clavier mapper en mode normal !"
  exit 0
else
  echo "Mauvaise entrée, au revoir !"
  exit 0
fi
