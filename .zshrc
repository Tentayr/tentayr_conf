#variable du chemin de oh my zsh
export ZSH=/home/crescents/.oh-my-zsh

#theme de oh my zsh
ZSH_THEME="ys"

#alias console zsh
alias zshconfig="$EDITOR ~/.zshrc"
alias ohmyzsh="$EDITOR ~/.oh-my-zsh"

#desactiver les titres automatique des terminaux
DISABLE_AUTO_TITLE="true"

#plugin list de ohmy zsh
plugins=(git git-flow vi-mode zsh-syntax-highlighting zsh-history-substring-search zsh-autosuggestions)

#source de oh my zsh
source $ZSH/oh-my-zsh.sh

# recommended by brew doctor
export PATH=$PATH:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin
export PATH='/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin'
export PATH=$PATH:$HOME/tool_kit

#chemin pour charger rbenv
export PATH="$HOME/.rbenv/bin:$PATH"

#init de rbenv
eval "$(rbenv init -)"

#definition de l'editeur preferer
export EDITOR='vim'

#definition des changues choisies
export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

#alias de tmux
alias tmux='tmux -2'

#source de nvm
source ~/.nvm/nvm.sh

#chargement de nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

#alias pour un ranger colorer
alias a='TERM=xterm-256color ranger'

#alias pour quitter la console etc
alias 'q'='exit'

#definition de certain chemin ?
PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
test -e ~/.dircolors && eval `dircolors -b ~/.dircolors`

#alias pour les ls
alias lsa="ls -a"
alias lsn="ls -n"
alias ls="ls --color=always"

#alias pour grep
alias grep="grep --color=always"
alias egrep="egrep --color=always"

#alias tmuxifier
alias t="tmuxifier s"

#alias bundle
alias beb="bundle exec middleman build"
alias besy="bundle exec middleman s3_sync"
alias bes="bundle exec middleman server -p"
alias bnd="bundle exec middleman build && bundle exec middleman s3_sync"

#clef d'activation/desactivation autosugestion
bindkey '^T' autosuggest-toggle

#chargement tmuxifier
export PATH="$HOME/.tmuxifier/bin:$PATH"
eval "$(tmuxifier init -)"

# tmux alias
alias tk="tmux kill-session -t"


#alias git
alias gbpurge='git branch --merged | grep -v "\*" | grep -v "master" | grep -v "develop" | grep -v "staging" | xargs -n 1 git branch -d'

#alias perso
alias keyboard="./Bureau/scriptshell/keyboard.sh "
alias yay=". ~/Bureau/scriptshell/git_good.sh "